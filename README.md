# RADICES SUBSTRATE MODULE

This repository includes all the necesary things to build a docker image to deploy a substrate private blockchain with radices extrinsics.

# Features included on this version of Lorena Substrate

- [x] Transfers
- [x] Enabled Democracy and Council Modules
    - [x] Do referendum proposals
    - [] Doesn't works all the Democracy features 
    - [] Council Proposals - Probably works in the next stable version
- [x] Enabled Radices Module
- [x] Enabled Staking
- [] LOR Token
    - [x] Using Substrate DEV token
    - [x] The transfers fails when use our own Token
- [x] Add New Validators/Nominators to the network
- [x] Runtime Online Upgrade
- [] The implementation of Radices Rust Library as external module doesn't works


# Dockerfile

To create the docker image we have kept these steps

1. Copy the substrate directory from lorena-substrate repository to /app image directory 
   
2. Install substrate and their dependencies.
   
3. Clone substrate repository. Using substrate stable version v1.0
[Parity Substrate Repository](https://github.com/paritytech/substrate)

4. Remove unnecesary folders and replace files with our specific implementation to include radices and chain spec configuration.

    - All the content of node/runtime/src directory and put our lib.rs and radices.rs files

    - The folder node-template

    - File /substrate/node/cli/src/chain_spec.rs 

    - Include our validators on initial_authorities variable

    - Add our account with all the tokens on endowed_accounts variable

    - Replace token units
    ```
    -       const STASH: u128 = 100 * DOLLARS;
    +       // const STASH: u128 = 100 * DOLLARS; // 10^16 = 10_000_000_000_000_000 = 10_000 TOKENS
    +       const STASH: u128 = 10000 * DOLLARS; // 10^18 = 1_000_000_000_000_000_000 = 1_000_000 TOKENS
    +       const STAKING: u128 = 100 * DOLLARS; // 10^16 = 10_000_000_000_000_000 = 10_000 TOKENS
    ```

    - Modify the minimum validator count to start the network from 4 to 2 (tesnet value) 
    ```
    +                       minimum_validator_count: 2,
    ```

    - Add the tokens staked to each initial authorities
    ```
    -                       stakers: initial_authorities.iter().map(|x| (x.0.clone(), x.1.clone(), STASH, StakerStatus::Validator)).collect(),
    +                       stakers: initial_authorities.iter().map(|x| (x.0.clone(), x.1.clone(), STAKING, StakerStatus::Validator)).collect(),
    ```

    - We found that constans `STASH`, `STAKING`  and `ENDOWMENT` to be too low. So we incremented their values from 20 to 50. 
    ```
    -       const STASH: u128 = 1 << 20;
    -       const ENDOWMENT: u128 = 1 << 20;
    +       // const STASH: u128 = 1 << 20;
    +       const STASH: u128 = 1 << 50;
    +       const STAKING: u128 = 1 << 48;****
    +       // const ENDOWMENT: u128 = 1 << 20;
    +       const ENDOWMENT: u128 = 1 << 50;
    ```

    - Set initial balances to endowment and authorities accounts
    ```
    -                       balances: endowed_accounts.iter().map(|k| (k.clone(), ENDOWMENT)).collect(),
    +                       // balances: endowed_accounts.iter().map(|k| (k.clone(), ENDOWMENT)).collect(),
    +                       balances: endowed_accounts.iter().cloned()
    +                               .map(|k| (k, ENDOWMENT))
    +                               .chain(initial_authorities.iter().map(|x| (x.0.clone(), STASH)))
    +                               .collect(),
    ```

    - Set staking values to our validators
    ```
    -                       stakers: initial_authorities.iter().map(|x| (x.0.clone(), x.1.clone(), STASH, StakerStatus::Validator)).collect(),
    +                       stakers: initial_authorities.iter().map(|x| (x.0.clone(), x.1.clone(), STAKING, StakerStatus::Validator)).collect(),
    ```

    - File /substrate/node/cli/Cargo.toml (Removed node-template calls and include radices)

    - File /substrate/srml/executive/src/lib.rs

    ** Modified the MAX_TRANSACTION_SIZE variable, was too low
    ```
    -       pub const MAX_TRANSACTIONS_SIZE: u32 = 4 * 1024 * 1024;
    +       pub const MAX_TRANSACTIONS_SIZE: u32 = 40 * 1024 * 1024;
    ```

    - File /app/scripts/common.sh ( Removed node-template calls )

5. Update rust toolchain `../scripts/init.sh`  
6. Build WASM binary `../scripts/build.sh`
7. Build Native Binary `cargo build --release`

# Dockerfile.dev

Now deprecated.

The Dockerfile.dev is the old version that we used to deploy a substrate node with radices substrate module and using DEV mode.