FROM debian:9.11 as builder
LABEL maintainer="Albert Valverde <albert@caelumlabs.com>"
LABEL description="Docker image to create a Radices Substrate Node"
# https://www.shawntabrizi.com/substrate-collectables-workshop/#/0/running-a-custom-node

# Installing tools
RUN apt-get update
RUN apt-get install -y git bash openssh-client curl grep build-essential apt-transport-https cmake clang pkg-config libssl-dev gcc libclang-dev

ENV DEBIAN_FRONTEND=noninteractive
ENV TERM=xterm
ENV PATH="$PATH:/root/.cargo/bin"

RUN ls -l 
COPY substrate /app
RUN ls -l /app
# Dependencies to install substrate and run nodes
WORKDIR /substrate

RUN curl https://getsubstrate.io -sSf | bash -s -- --fast
RUN git clone -b v1.0 https://github.com/paritytech/substrate .
# RUN git clone --branch v1.0 https://github.com/substrate-developer-hub/substrate-package
RUN cd ./node && \
  rm -rf /substrate/node/runtime/src/* && \
  rm -rf /substrate/node-template && \
  rm -f /substrate/node/cli/src/chain_spec.rs && \
  rm -f /substrate/node/cli/Cargo.toml && \
  rm -f /substrate/srml/executive/src/lib.rs && \
  cp -R /app/scripts/common.sh /substrate/scripts/. && \
  cp -R /app/Cargo.toml /substrate/. && \
  cp -R /app/node/* /substrate/node/. && \
  cp -R /app/srml/executive/src/lib.rs /substrate/srml/executive/src/lib.rs && \
  ../scripts/init.sh && \
  ../scripts/build.sh && \
  cargo build --release

RUN ls -l /substrate/node/runtime/src

# ===== SECOND STAGE ======

FROM debian:9.11
LABEL maintainer="Albert Valverde <albert@caelumlabs.com>"
LABEL description="Docker image to create a Radices Substrate Node"

# Installing tools
RUN apt-get update
RUN apt-get install -y git bash openssh-client curl grep vim nmon net-tools build-essential lsb-release apt-transport-https cmake clang pkg-config libssl-dev gcc libclang-dev

ENV PATH="$PATH:/root/.cargo/bin"

RUN useradd -m -u 1000 -U -s /bin/sh -d /substrate substrate

COPY --from=builder /substrate /substrate

WORKDIR /substrate
USER substrate
EXPOSE 30333 9933 9944

# Running a check 
RUN ./target/release/substrate --version

# Start the node
ENTRYPOINT [ "./target/release/substrate" ]
